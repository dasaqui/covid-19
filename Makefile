grafica: scripts/updateCheck R/*.R imgs/g_mun.jpg imgs/g_amL.jpg imgs/g_nac.jpg imgs/g_est.jpg
	rm imgs/g_*-1.jpg
	rm tmp.jpg

scripts/update: scripts/updateGit.sh scripts/updateGob.sh
	scripts/updateGit.sh
	scripts/updateGob.sh
	@touch scripts/update

scripts/updateCheck: scripts/update
	@find scripts/update -mmin +180 -delete && make scripts/update

imgs/g_mun.jpg: R/graf*.R R/mundial.R R/plotPack.R R/pronostico.R fl00/*.csv
	R -f R/mundial.R
	montage imgs/R\ mun*.jpg -geometry -0-0 tmp.jpg
	convert tmp.jpg  xc: -stroke "#77a0bf"  -fill "#90c2e7"  -strokewidth 2 -draw 'circle 0,800 20,800' imgs/g_mun.jpg

imgs/g_nac.jpg: R/graf*.R R/nacional.R R/plotPack.R R/pronostico.R fl01/*.csv
	R -f R/nacional.R
	montage imgs/R\ nac*.jpg -geometry -0-0 tmp.jpg
	convert tmp.jpg  xc: -stroke "#84587F"  -fill "#A06B9A"  -strokewidth 2 -draw 'circle 0,800 20,800' imgs/g_nac.jpg

imgs/g_amL.jpg: R/graf*.R R/amLatina.R R/plotPack.R R/pronostico.R fl00/*.csv
	R -f R/amLatina.R
	montage imgs/R\ amL*.jpg -geometry -0-0 tmp.jpg
	convert tmp.jpg  xc: -stroke "#b4b4b3"  -fill "#dadad9"  -strokewidth 2 -draw 'circle 0,800 20,800' imgs/g_amL.jpg

imgs/g_est.jpg: R/graf*.R R/estatal.R R/plotPack.R R/pronostico.R fl00/*.csv
	R -f R/estatal.R
	montage imgs/R\ est*.jpg -geometry -0-0 tmp.jpg
	convert tmp.jpg  xc: -stroke "#d09149"  -fill "#fcaf58"  -strokewidth 2 -draw 'circle 0,800 20,800' imgs/g_est.jpg
