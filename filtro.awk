BEGIN {
    print "\"dia\",\"fecha\",\""pais"_c\",\""pais"_d\",\""pais"_r\",\""pais"\""
    FPAT = "([^,]*)|(\"[^\"]+\")"
    #FS = ","
    last = ""
    suma1 = 0
    suma2 = 0
    suma3 = 0
    dia = 0
}

# Revisando cual columna debo usar
/Province\/State/ || /Last Update/ {
    for (i = 1; i <= NF; i++){
	switch( $i){
	    case /Country\/Region/:
		var1=i;
		break
	    case /Confirmed/:
		var2=i;
		break
	    case /Deaths/:
		var3=i;
		break
	    case /Recovered/:
		var4=i;
		break
	}
    }
}

$var1 == pais {
    last  = fl
    suma1 = suma1 + $var2
    suma2 = suma2 + $var3
    suma3 = suma3 + $var4
}

/COVID-19/ {
    fl = substr($0, 60, 10);
    sum = suma1 - week[dia%14];

    print dia "," fl "," suma1 "," suma2 "," suma3 "," sum
    
    week[dia%14] = suma1
    suma1 = 0;
    suma2 = 0;
    suma3 = 0;
    dia = dia+1
}
