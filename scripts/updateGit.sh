#!/bin/bash

########################################
# Actualizacion de datos git
(
    echo Actualizando datos
    cd ..
    cd COVID-19
    git pull
)

########################################
# Resumen de datos del mundo (por pais)
echo "" > concat.csv
for f in ../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/*.csv
do ( cat "${f}"| sed "s/_/\//g"; echo; echo ${f}) >> concat.csv
done

(mkdir fl00)

sed -i "s/Mainland China/China/g" concat.csv
cat concat.csv| awk -f filtro.awk -v pais="China"| tee fl00/cn.csv| head
cat concat.csv| awk -f filtro.awk -v pais="Italy"| tee fl00/it.csv| head
cat concat.csv| awk -f filtro.awk -v pais="Mexico"| tee fl00/mx.csv| head
cat concat.csv| awk -f filtro.awk -v pais="Spain"| tee fl00/sp.csv| head
cat concat.csv| awk -f filtro.awk -v pais="US"| tee fl00/us.csv| head

cat concat.csv| awk -f filtro.awk -v pais="Brazil"| tee fl00/bz.csv| head
cat concat.csv| awk -f filtro.awk -v pais="Peru"| tee fl00/pe.csv| head

(mkdir imgs;mkdir gnuplot) 2>/dev/null
for f in gnuplot/*.gpt
do
    gnuplot -c ${f} -p
done


montage imgs/**.jpg -geometry -0-0 collage.jpg
