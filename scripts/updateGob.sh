#!/bin/bash

########################################
# Actualizacion de datos mexico
(
    cd datamx
    wget -rl 1 https://serendipia.digital/2020/03/datos-abiertos-sobre-casos-de-coronavirus-covid-19-en-mexico/ -nd -nc -A "*.csv"
)

########################################
# Resumen de datos de méxico por día
( mkdir fl01)
for f in datamx/*positivos*csv
do
    ff=`echo $f| sed "s/.*2020/2020/g"| sed "s/-Tab.*//g"| sed "s/\./-/g"`
    echo $ff

    [ -f fl01/${ff}.csv ] && continue

    sed -i "y/ÁÉÍÓÚ/AEIOU/" $f
    sed -i "s/DISTRITO FEDERAL/CIUDAD DE MEXICO/g" $f
    awk -f porEstado.awk -v fecha=$ff estados.txt $f > fl01/diario${ff}.csv
done
rm fl01/diario*-*-*-*csv

csvjoin -c fecha fl01/diario*.csv > fl01/tmp1.csv
ruby -rcsv -e 'puts CSV.parse(STDIN).transpose.map &:to_csv'\
     < fl01/tmp1.csv >\
     fl01/tmp2.csv

campos=`head -1 fl01/tmp1.csv | sed 's/[^,]//g' | wc -c`
sed -i "s/_c,/_p,,,,,,,,,,,,,,/g" fl01/tmp1.csv
cut -d, -f -$campos fl01/tmp1.csv > fl01/tmp3.csv
ruby -rcsv -e 'puts CSV.parse(STDIN).transpose.map &:to_csv'\
     < fl01/tmp3.csv >\
     fl01/tmp4.csv

csvjoin -c fecha -l fl01/tmp2.csv fl01/tmp4.csv > fl01/compilado.csv
rm fl01/tmp*csv
