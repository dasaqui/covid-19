BEGIN {
    FS = ","
    OFS= ","
    print "fecha", fecha
}

FILENAME=="estados.txt" {
    total[$1]=0;
    next
}

/Corte/ { next}
/Estado/{ next}

NF > 5 {
    total[$2]++;
    suma[$2,$6]++;
}

END {
    for (key in total){
	print key"_c", total[key]
    }
}
